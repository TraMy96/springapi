package com.pigmy.springbootapi.controller;
import com.pigmy.springbootapi.model.AccountInfo;
import com.pigmy.springbootapi.model.AlbumInfo;
import com.pigmy.springbootapi.model.GenreInfo;
import com.pigmy.springbootapi.service.AccountService;
import com.pigmy.springbootapi.service.AlbumService;
import com.pigmy.springbootapi.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by nhs3108 on 16/03/2017.
 */
@RestController
public class MainController {
    @Autowired
    AccountService accountService;
    @Autowired
    AlbumService albumService;
    @Autowired
    GenreService genreService;
    @RequestMapping(value = { "/users" }, method = RequestMethod.GET)
    @ResponseBody
    public List<GenreInfo> users() {
        List<GenreInfo> albumInfo = genreService.getListGenre();
        return albumInfo;
    }
}