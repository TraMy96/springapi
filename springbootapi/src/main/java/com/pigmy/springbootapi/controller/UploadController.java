package com.pigmy.springbootapi.controller;

import com.pigmy.springbootapi.model.AlbumInfo;
import com.pigmy.springbootapi.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.UrlResource;
import org.springframework.core.io.Resource;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.File;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/upload")
public class UploadController {

    @Autowired
    AlbumService albumService;
    AlbumInfo albumInfo = new AlbumInfo();
    List<String> files = new ArrayList<String>();
    @PostMapping("/post")
    public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file) {
        String message = "";
        try {
             albumInfo = new AlbumInfo();
            albumInfo.setFileData(file);
            albumService.uploadImage(albumInfo);
            files.add(file.getOriginalFilename());

            message = albumInfo.getImageUrl();
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
            message = "FAIL to upload " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @GetMapping("/getallfiles")
    public ResponseEntity<List<String>> getListFiles(Model model, @RequestParam("filename") String filename) {
        List<String> filesParam = new ArrayList<String>();
        filesParam.add(filename);
        List<String> fileNames = filesParam
                .stream().map(fileName -> MvcUriComponentsBuilder
                        .fromMethodName(UploadController.class, "getFile", fileName).build().toString())
                .collect(Collectors.toList());

        return ResponseEntity.ok().body(fileNames);
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = this.loadFile(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    public Resource loadFile(String filename) {
        try {
            String uploadRootPath = "images" + File.separator + filename;
            File uploadRootDir = new File(uploadRootPath);
            Path file = Paths.get(uploadRootDir.getAbsolutePath());
            //Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("FAIL!");
        }
    }
   /* @RequestMapping(value = "/getImage/{filename:.+}", method = RequestMethod.GET)
    public ResponseEntity<Resource> getImage(@PathVariable String filename) {

        try {
            String uploadRootPath = "images" + File.separator + filename;
            File uploadRootDir = new File(uploadRootPath);
            Path file = Paths.get(uploadRootDir.getAbsolutePath());
            String path = file.toString();
            Resource loader = resourceLoader.getResource("file:" + path);
            return new ResponseEntity<Resource>(loader, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Resource>(HttpStatus.NOT_FOUND);
        }
    }*/
}
