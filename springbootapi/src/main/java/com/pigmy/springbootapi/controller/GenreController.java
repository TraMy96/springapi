package com.pigmy.springbootapi.controller;

import com.pigmy.springbootapi.model.GenreInfo;
import com.pigmy.springbootapi.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/genre")
public class GenreController {
    @Autowired
    GenreService genreService;
    @RequestMapping(value = { "/genreList" }, method = RequestMethod.GET)
    @ResponseBody
    public List<GenreInfo> genreList() {
        List<GenreInfo> genreInfos = genreService.getListGenre();
        return genreInfos;
    }
}
