package com.pigmy.springbootapi.controller;

import com.pigmy.springbootapi.config.TokenAuthenticationService;
import com.pigmy.springbootapi.entity.Account;
import com.pigmy.springbootapi.model.AlbumInfo;
import com.pigmy.springbootapi.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/account")
public class AccountController {
    @Autowired
    AccountService accountService;

    @RequestMapping(value = { "/accountDetail" }, method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAccountDetail(HttpServletRequest request) {
        Map<String, Object> accountInfo = new HashMap<String, Object>();
        Authentication accountDetail = TokenAuthenticationService.getAuthentication(request);
        Account account = accountService.findAccount(accountDetail.getName());
        accountInfo.put("username", account.getUserName());
        accountInfo.put("email", account.getEmail());
        Map<String, Object> role = new HashMap<String, Object>();
        role.put("id", account.getUserRole().getID());
        role.put("name", account.getUserRole().getName());
        accountInfo.put("role", role);
        return accountInfo;
    }

}
