package com.pigmy.springbootapi.controller;

import com.pigmy.springbootapi.entity.Album;
import com.pigmy.springbootapi.entity.Artist;
import com.pigmy.springbootapi.entity.Genre;
import com.pigmy.springbootapi.model.AlbumInfo;
import com.pigmy.springbootapi.model.ArtistInfo;
import com.pigmy.springbootapi.model.GenreInfo;
import com.pigmy.springbootapi.service.AlbumService;
import com.pigmy.springbootapi.validator.AlbumInfoValidator;
import com.sun.javafx.collections.MappingChange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import javax.validation.Valid;
import java.util.*;
import java.util.HashMap;
import java.util.Map.Entry;

@RestController
@RequestMapping("/api/album")
public class AlbumController {
    @Autowired
    AlbumService albumService;
    @Autowired
    AlbumInfoValidator albumInfoValidator;
    @InitBinder
    public void myInitBinder(WebDataBinder dataBinder) {
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
        System.out.println("Target=" + target);

        if (target.getClass() == AlbumInfo.class) {
            dataBinder.setValidator(albumInfoValidator);
            dataBinder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        }
    }

    @RequestMapping(value = { "/albumList" }, method = RequestMethod.GET)
    @ResponseBody
    public List<Map<String, Object>> getListAlbum() {
        List<Map<String, Object>> albumList = new ArrayList();
        List<AlbumInfo> albumInfo = albumService.getListAlbum();
        for(int i = 0; i < albumInfo.size(); i++){
            Map<String, Object> album = new HashMap<String, Object>();
            album.put("id", albumInfo.get(i).getID());

            Map<String, Object> genreInfo = new HashMap<String, Object>();
            genreInfo.put("id", albumInfo.get(i).getGenre().getID());
            genreInfo.put("name", albumInfo.get(i).getGenre().getName());
            genreInfo.put("description", albumInfo.get(i).getGenre().getDescription());
            album.put("genre", genreInfo);

            Map<String, Object> artistInfo = new HashMap<String, Object>();
            artistInfo.put("id", albumInfo.get(i).getArtist().getID());
            artistInfo.put("name", albumInfo.get(i).getArtist().getName());
            album.put("artist", artistInfo);

            album.put("title", albumInfo.get(i).getTitle());
            album.put("price", albumInfo.get(i).getPrice());
            album.put("imageUrl", albumInfo.get(i).getImageUrl());
            album.put("albumID", albumInfo.get(i).getAlbumID());
            album.put("genreName", albumInfo.get(i).getGenreName());
            album.put("artistName", albumInfo.get(i).getArtistName());
            album.put("genreID", albumInfo.get(i).getGenreID());
            album.put("artistID", albumInfo.get(i).getArtistID());
            album.put("priceString", albumInfo.get(i).getPriceString());

            Map<String, Object> albumDetail = new HashMap<String, Object>();
            albumDetail.put("album", album);
            albumList.add(albumDetail);
        }
        return albumList;
    }

    @RequestMapping(value = { "/albumdetail" }, method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAlbumDetail(@RequestParam(value = "id", defaultValue = "") String id) {
        AlbumInfo albumInfo = albumService.getAlbumDetail(id);
            Map<String, Object> album = new HashMap<String, Object>();
            album.put("id", albumInfo.getID());

            Map<String, Object> genreInfo = new HashMap<String, Object>();
            genreInfo.put("id", albumInfo.getGenre().getID());
            genreInfo.put("name", albumInfo.getGenre().getName());
            genreInfo.put("description", albumInfo.getGenre().getDescription());
            album.put("genre", genreInfo);

            Map<String, Object> artistInfo = new HashMap<String, Object>();
            artistInfo.put("id", albumInfo.getArtist().getID());
            artistInfo.put("name", albumInfo.getArtist().getName());
            album.put("artist", artistInfo);

            album.put("title", albumInfo.getTitle());
            album.put("price", albumInfo.getPrice());
            album.put("imageUrl", albumInfo.getImageUrl());
            album.put("albumID", albumInfo.getAlbumID());
            album.put("genreName", albumInfo.getGenreName());
            album.put("artistName", albumInfo.getArtistName());
            album.put("genreID", albumInfo.getGenreID());
            album.put("artistID", albumInfo.getArtistID());
            album.put("priceString", albumInfo.getPriceString());

            Map<String, Object> albumDetail = new HashMap<String, Object>();
            albumDetail.put("album", album);
        return album;
    }

    @RequestMapping(value = { "/delete" }, method = RequestMethod.DELETE)
    @ResponseBody
    public  Map<String, Object> deleteAlbum(Model model, @RequestParam(value = "id", defaultValue = "") String id) {
        Map<String, Object> result = new HashMap<String, Object>();
        if(albumService.deleteAlbum(id) == false){
            result.put("result", false);
            return result;
        }
        result.put("result", true);
        return result;
    }

    @RequestMapping(value = { "/add" }, method = RequestMethod.POST)
    @ResponseBody
    @Transactional(propagation = Propagation.NEVER)
    public Map<String, Object> addAlbum(Model model,
                           @Valid @RequestBody AlbumInfo albumInfo,
                           BindingResult result) {

        Map<String, Object> status = new HashMap<String, Object>();

        if (result.hasErrors()) {
            status.put("result", result.getAllErrors());
            return status;
        }
        else{
            if(albumService.addAlbum(albumInfo) == null){
                status.put("result", "Save Error!");
                return status;
            }

            Album albumLast = albumService.getAlbumLast();
            Map<String, Object> album = new HashMap<String, Object>();
            album.put("id", albumLast.getID());

            Map<String, Object> genreInfo = new HashMap<String, Object>();
            genreInfo.put("id", albumLast.getGenre().getID());
            genreInfo.put("name", albumLast.getGenre().getName());
            genreInfo.put("description", albumLast.getGenre().getDescription());
            album.put("genre", genreInfo);

            Map<String, Object> artistInfo = new HashMap<String, Object>();
            artistInfo.put("id", albumLast.getArtist().getID());
            artistInfo.put("name", albumLast.getArtist().getName());
            album.put("artist", artistInfo);

            album.put("title", albumLast.getTitle());
            album.put("price", albumLast.getPrice());
            album.put("imageUrl", albumLast.getImageUrl());
            album.put("priceString", String.valueOf(albumLast.getPrice()));
            return album;
        }
    }

    @RequestMapping(value = { "/edit" }, method = RequestMethod.PUT)
    @ResponseBody
    @Transactional(propagation = Propagation.NEVER)
    public Map<String, Object> editAlbum(Model model,
                                         @RequestParam(value = "id", defaultValue = "") String id,
                                        @Valid @RequestBody AlbumInfo albumInfo,
                                        BindingResult result) {

        Map<String, Object> status = new HashMap<String, Object>();

        if (result.hasErrors()) {
            status.put("result", result.getAllErrors());
            return status;
        }
        else{
            if(albumService.editAlbum(albumInfo, id) == null){
                status.put("result", "Save Error!");
                return status;
            }

            AlbumInfo albumLast = albumService.getAlbumDetail(id);
            Map<String, Object> album = new HashMap<String, Object>();
            album.put("id", albumLast.getID());

            Map<String, Object> genreInfo = new HashMap<String, Object>();
            genreInfo.put("id", albumLast.getGenre().getID());
            genreInfo.put("name", albumLast.getGenre().getName());
            genreInfo.put("description", albumLast.getGenre().getDescription());
            album.put("genre", genreInfo);

            Map<String, Object> artistInfo = new HashMap<String, Object>();
            artistInfo.put("id", albumLast.getArtist().getID());
            artistInfo.put("name", albumLast.getArtist().getName());
            album.put("artist", artistInfo);

            album.put("title", albumLast.getTitle());
            album.put("price", albumLast.getPrice());
            album.put("imageUrl", albumLast.getImageUrl());
            album.put("priceString", String.valueOf(albumLast.getPrice()));
            return album;
        }
    }

    @RequestMapping(value = { "/upload" }, method = RequestMethod.POST, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE})
    @ResponseBody
    public Map<String, Object> upload(@RequestBody MultipartFile file) {

        Map<String, Object> status = new HashMap<String, Object>();

        AlbumInfo albumInfo = new AlbumInfo();
        albumInfo.setFileData(file);
        albumService.uploadImage(albumInfo);

        status.put("result", true);
        return status;

    }
}
