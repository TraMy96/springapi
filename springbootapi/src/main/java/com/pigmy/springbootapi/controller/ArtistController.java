package com.pigmy.springbootapi.controller;

import com.pigmy.springbootapi.model.ArtistInfo;
import com.pigmy.springbootapi.model.GenreInfo;
import com.pigmy.springbootapi.service.ArtistService;
import com.pigmy.springbootapi.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/artist")
public class ArtistController {
    @Autowired
    ArtistService artistService;
    @RequestMapping(value = { "/artistList" }, method = RequestMethod.GET)
    @ResponseBody
    public List<ArtistInfo> artistList() {
        List<ArtistInfo> artistList = artistService.getListArtist();
        return artistList;
    }
}
