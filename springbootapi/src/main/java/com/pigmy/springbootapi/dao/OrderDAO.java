package com.pigmy.springbootapi.dao;

import com.pigmy.springbootapi.entity.Account;
import com.pigmy.springbootapi.entity.Order;
import com.pigmy.springbootapi.model.OrderInfo;

import java.util.List;

public interface OrderDAO {
	public Order findOrder(int orderID);
	public void save(OrderInfo orderInfo);
	public Order getOrderLast();
	public List<OrderInfo> getOrderListByAccount(Account account);
	public OrderInfo getOrderInfo(int orderID);
}
