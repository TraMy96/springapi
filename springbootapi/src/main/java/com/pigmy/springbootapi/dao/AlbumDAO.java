package com.pigmy.springbootapi.dao;

import com.pigmy.springbootapi.entity.Album;
import com.pigmy.springbootapi.model.AlbumInfo;

import java.util.List;

public interface AlbumDAO {
	public List<AlbumInfo> getListAlbum();
	public Album findAlbum(int albumID);
	public AlbumInfo findAlbumDetail(int albumID);
	public void save(AlbumInfo albumInfo);
	public void delete(AlbumInfo albumInfo);
	public Album getAlbumLast();
}
