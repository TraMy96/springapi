package com.pigmy.springbootapi.dao;

import com.pigmy.springbootapi.entity.Artist;
import com.pigmy.springbootapi.model.ArtistInfo;

import java.util.List;

public interface ArtistDAO {
	public Artist findArtist(int id);
	public List<ArtistInfo> getListArtist();
	public ArtistInfo findArtistDetail(int artistID);
	public void save(ArtistInfo artistInfo);
}
