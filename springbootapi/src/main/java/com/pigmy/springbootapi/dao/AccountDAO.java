package com.pigmy.springbootapi.dao;

import com.pigmy.springbootapi.entity.Account;
import com.pigmy.springbootapi.model.AccountInfo;

public interface AccountDAO {

   
   public Account findAccount(String userName);
   public void save(AccountInfo accountInfo);
}