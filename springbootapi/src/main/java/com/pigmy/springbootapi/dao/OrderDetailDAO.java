package com.pigmy.springbootapi.dao;

import com.pigmy.springbootapi.entity.Order;
import com.pigmy.springbootapi.entity.OrderDetail;
import com.pigmy.springbootapi.model.OrderDetailInfo;

import java.util.List;

public interface OrderDetailDAO {
	public OrderDetail findOrderDetail(int orderDetailID);
	public void save(OrderDetailInfo orderDetailInfo);
	public List<OrderDetailInfo> getListOrderDetailByOrder(Order order);
}
