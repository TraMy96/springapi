package com.pigmy.springbootapi.dao;

import com.pigmy.springbootapi.entity.Role;

public interface RoleDAO {
    public Role findRole(int id);
}
