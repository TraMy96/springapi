package com.pigmy.springbootapi.dao;

import com.pigmy.springbootapi.entity.Genre;
import com.pigmy.springbootapi.model.AlbumInfo;
import com.pigmy.springbootapi.model.GenreInfo;

import java.util.List;

public interface GenreDAO {
	public List<GenreInfo> getListGenre();
	public List<AlbumInfo> getListAlbumByGenre(int genreID);
	public Genre findGenre(int genreID);
	public GenreInfo findGenreDetail(int genreID);
	public void save(GenreInfo genreInfo);
}
