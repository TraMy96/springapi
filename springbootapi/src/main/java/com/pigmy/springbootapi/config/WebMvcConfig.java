package com.pigmy.springbootapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.*;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebMvc
public class WebMvcConfig extends WebMvcConfigurerAdapter {


  @Override
  public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
      configurer.enable();
  }

  /*@Bean
    public WebMvcConfigurer corsConfigurer(){
      return new WebMvcConfigurerAdapter(){
          @Override
          public void addCorsMappings(CorsRegistry corsRegistry){
              corsRegistry.addMapping("/**").allowedOrigins("*").
                      allowedMethods("*");
          }
      };
  }*/
}