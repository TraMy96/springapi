package com.pigmy.springbootapi.service;

import com.pigmy.springbootapi.model.ArtistInfo;

import java.util.List;

public interface ArtistService {
    public List<ArtistInfo> getListArtist();
    public ArtistInfo addArtist(ArtistInfo artistInfo);
}
