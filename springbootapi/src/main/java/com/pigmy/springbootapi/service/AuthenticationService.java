package com.pigmy.springbootapi.service;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AuthenticationService {
    public void addAuthentication(HttpServletResponse res, String username);
    public Authentication getAuthentication(HttpServletRequest request);
}
