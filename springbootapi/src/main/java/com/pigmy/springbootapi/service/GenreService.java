package com.pigmy.springbootapi.service;

import com.pigmy.springbootapi.model.GenreInfo;

import java.util.List;

public interface GenreService {
    public List<GenreInfo> getListGenre();
    public GenreInfo addGenre(GenreInfo genreInfo);
}
