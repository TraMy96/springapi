package com.pigmy.springbootapi.service;

import com.pigmy.springbootapi.entity.Order;
import com.pigmy.springbootapi.model.OrderDetailInfo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface OrderDetailService {
    public List<OrderDetailInfo> addOrderDetail(Order order, HttpServletRequest request);
    public List<OrderDetailInfo> getListOrderDetailByOrder(String orderID);
}
