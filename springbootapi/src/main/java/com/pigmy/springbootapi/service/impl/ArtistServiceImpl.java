package com.pigmy.springbootapi.service.impl;

import com.pigmy.springbootapi.dao.ArtistDAO;
import com.pigmy.springbootapi.model.ArtistInfo;
import com.pigmy.springbootapi.service.ArtistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArtistServiceImpl implements ArtistService{
    @Autowired
    ArtistDAO artistDAO;

    @Override
    public List<ArtistInfo> getListArtist(){
        return artistDAO.getListArtist();
    }

    @Override
    public ArtistInfo addArtist(ArtistInfo artistInfo){
        try {
            artistDAO.save(artistInfo);
        } catch (Exception e) {
            return null;
        }
        return artistInfo;
    }
}
