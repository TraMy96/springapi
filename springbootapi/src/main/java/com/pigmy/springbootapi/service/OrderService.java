package com.pigmy.springbootapi.service;

import com.pigmy.springbootapi.entity.Order;
import com.pigmy.springbootapi.model.OrderInfo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface OrderService {
    public OrderInfo addOrder(OrderInfo orderInfo, HttpServletRequest request);
    public Order getOrderLast();
    public List<OrderInfo> getOrderListByAccount();
    public OrderInfo getOrderInfo(String id);
}
