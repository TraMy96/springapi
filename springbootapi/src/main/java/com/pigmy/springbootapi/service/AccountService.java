package com.pigmy.springbootapi.service;

import com.pigmy.springbootapi.entity.Account;
import com.pigmy.springbootapi.model.AccountInfo;

public interface AccountService {
    public boolean isLogin();
    public AccountInfo getInfoUserLoggedIn();
    public AccountInfo register(AccountInfo accountInfo);
    public Account getUserLoggedIn();
    public Account findAccount(String username);
}
