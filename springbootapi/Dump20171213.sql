-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: musicstore_db
-- ------------------------------------------------------
-- Server version	5.6.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `User_Name` varchar(20) NOT NULL,
  `Active` bit(1) NOT NULL,
  `Password` varchar(60) CHARACTER SET utf8 NOT NULL,
  `Role_ID` int(11) NOT NULL,
  `Email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`User_Name`),
  KEY `ACCOUNT_ROLE_FK` (`Role_ID`),
  CONSTRAINT `ACCOUNT_ROLE_FK` FOREIGN KEY (`Role_ID`) REFERENCES `roles` (`Role_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES ('cus1','','$2a$10$htKioLhtKvEF2Xn6iEFWq.g88A/TkyAyxyoYwKIJw24yQrTmEukSm',1,'123@gmail.com'),('cus2','','$2a$10$1bzO/tzBeyMOLlgCWj8cyeEQ7z4ZBFT65CkoBeMbhBm72gW/7krHu',1,'cus2@gmail.com'),('cus3','','$2a$10$flQhTNRO5np1Wg6kZXlfvOMpIWGdbhDyfjMFHAIJ3XJiY8kFvKM/u',1,'cus3@gmail.com'),('cus4','','$2a$10$U9dBHH1kSlPk4rX4ReFaBeEitAljCq6ixMvI/kg8F4ZuYPXBnBLje',1,'cus4@gmail.com'),('nv1','','$2a$10$C8ImCu2zbhnu4GiTpzPxVOppjNx7jYVfiwBm2kOlh9WnMe4uCe0wi',1,'123@gmail.com'),('ql1','','$2a$10$8Cwo.DB4W7MgJA.tFMBTdeLGMnjyBFb.LrCo/DikHs6rqshHiunDq',2,'abc@gmail.com'),('ql2','','$2a$10$bgsjWJTMg7C9BtKyLIXljuFLTqLfd8Rt01oxCZV/Z/m1Z6c/jRh8G',2,'def@gmail.com');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums` (
  `Album_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Genre_ID` int(11) NOT NULL,
  `Artist_ID` int(11) NOT NULL,
  `Title` varchar(160) CHARACTER SET utf8 NOT NULL,
  `Price` double NOT NULL,
  `Image_Url` varchar(1024) CHARACTER SET utf8 NOT NULL DEFAULT 'albumdefault.jpg',
  PRIMARY KEY (`Album_ID`),
  KEY `ALBUM_GENRE_FK` (`Genre_ID`),
  KEY `ALBUM_ARTIST_FK` (`Artist_ID`),
  CONSTRAINT `ALBUM_ARTIST_FK` FOREIGN KEY (`Artist_ID`) REFERENCES `artists` (`Artist_ID`),
  CONSTRAINT `ALBUM_GENRE_FK` FOREIGN KEY (`Genre_ID`) REFERENCES `genres` (`Genre_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=674 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `albums`
--

LOCK TABLES `albums` WRITE;
/*!40000 ALTER TABLE `albums` DISABLE KEYS */;
INSERT INTO `albums` VALUES (386,2,1,'For Those About To Rock We Salute You',8.99,'albumdefault.jpg'),(387,1,1,'Let There Be Rock',8.99,'albumdefault.jpg'),(388,1,100,'Greatest Hits',8.99,'albumdefault.jpg'),(389,1,102,'Misplaced Childhood',8.99,'albumdefault.jpg'),(390,1,105,'The Best Of Men At Work',8.99,'albumdefault.jpg'),(392,1,110,'Nevermind',8.99,'albumdefault.jpg'),(393,1,111,'Compositores',8.99,'albumdefault.jpg'),(394,1,114,'Bark at the Moon (Remastered)',8.99,'albumdefault.jpg'),(395,1,114,'Blizzard of Ozz',8.99,'albumdefault.jpg'),(396,1,114,'Diary of a Madman (Remastered)',8.99,'albumdefault.jpg'),(397,1,114,'No More Tears (Remastered)',8.99,'albumdefault.jpg'),(398,1,114,'Speak of the Devil',8.99,'albumdefault.jpg'),(399,1,115,'Walking Into Clarksdale',8.99,'albumdefault.jpg'),(400,1,117,'The Beast Live',8.99,'albumdefault.jpg'),(401,1,118,'Live On Two Legs [Live]',8.99,'albumdefault.jpg'),(402,1,118,'Riot Act',8.99,'albumdefault.jpg'),(403,1,118,'Ten',8.99,'albumdefault.jpg'),(404,1,118,'Vs.',8.99,'albumdefault.jpg'),(405,1,120,'Dark Side Of The Moon',8.99,'albumdefault.jpg'),(406,1,124,'New Adventures In Hi-Fi',8.99,'albumdefault.jpg'),(407,1,126,'Raul Seixas',8.99,'albumdefault.jpg'),(408,1,127,'By The Way',8.99,'albumdefault.jpg'),(409,1,127,'Californication',8.99,'albumdefault.jpg'),(410,1,128,'Retrospective I (1974-1980)',8.99,'albumdefault.jpg'),(411,1,130,'Maquinarama',8.99,'albumdefault.jpg'),(412,1,130,'O Samba Poconé',8.99,'albumdefault.jpg'),(413,1,132,'A-Sides',8.99,'albumdefault.jpg'),(414,1,134,'Core',8.99,'albumdefault.jpg'),(415,1,136,'[1997] Black Light Syndrome',8.99,'albumdefault.jpg'),(416,1,139,'Beyond Good And Evil',8.99,'albumdefault.jpg'),(418,1,140,'The Doors',8.99,'albumdefault.jpg'),(419,1,141,'The Police Greatest Hits',8.99,'albumdefault.jpg'),(420,1,142,'Hot Rocks, 1964-1971 (Disc 1)',8.99,'albumdefault.jpg'),(421,1,142,'No Security',8.99,'albumdefault.jpg'),(422,1,142,'Voodoo Lounge',8.99,'albumdefault.jpg'),(423,1,144,'My Generation - The Very Best Of The Who',8.99,'albumdefault.jpg'),(424,1,150,'Achtung Baby',8.99,'albumdefault.jpg'),(425,1,150,'B-Sides 1980-1990',8.99,'albumdefault.jpg'),(426,1,150,'How To Dismantle An Atomic Bomb',8.99,'albumdefault.jpg'),(427,1,150,'Pop',8.99,'albumdefault.jpg'),(428,1,150,'Rattle And Hum',8.99,'albumdefault.jpg'),(429,1,150,'The Best Of 1980-1990',8.99,'albumdefault.jpg'),(430,1,150,'War',8.99,'albumdefault.jpg'),(431,1,150,'Zooropa',8.99,'albumdefault.jpg'),(432,1,152,'Diver Down',8.99,'albumdefault.jpg'),(433,1,152,'The Best Of Van Halen, Vol. I',8.99,'albumdefault.jpg'),(434,1,152,'Van Halen III',8.99,'albumdefault.jpg'),(435,1,152,'Van Halen',8.99,'albumdefault.jpg'),(436,1,153,'Contraband',8.99,'albumdefault.jpg'),(437,1,157,'Un-Led-Ed',8.99,'albumdefault.jpg'),(439,1,2,'Balls to the Wall',8.99,'albumdefault.jpg'),(440,1,2,'Restless and Wild',8.99,'albumdefault.jpg'),(441,1,200,'Every Kind of Light',8.99,'albumdefault.jpg'),(442,1,22,'BBC Sessions [Disc 1] [Live]',8.99,'albumdefault.jpg'),(443,1,22,'BBC Sessions [Disc 2] [Live]',8.99,'albumdefault.jpg'),(444,1,22,'Coda',8.99,'albumdefault.jpg'),(445,1,22,'Houses Of The Holy',8.99,'albumdefault.jpg'),(446,1,22,'In Through The Out Door',8.99,'albumdefault.jpg'),(447,1,22,'IV',8.99,'albumdefault.jpg'),(448,1,22,'Led Zeppelin I',8.99,'albumdefault.jpg'),(449,1,22,'Led Zeppelin II',8.99,'albumdefault.jpg'),(450,1,22,'Led Zeppelin III',8.99,'albumdefault.jpg'),(451,1,22,'Physical Graffiti [Disc 1]',8.99,'albumdefault.jpg'),(452,1,22,'Physical Graffiti [Disc 2]',8.99,'albumdefault.jpg'),(453,1,22,'Presence',8.99,'albumdefault.jpg'),(454,1,22,'The Song Remains The Same (Disc 1)',8.99,'albumdefault.jpg'),(455,1,22,'The Song Remains The Same (Disc 2)',8.99,'albumdefault.jpg'),(456,1,23,'Bongo Fury',8.99,'albumdefault.jpg'),(457,1,3,'Big Ones',8.99,'albumdefault.jpg'),(458,1,4,'Jagged Little Pill',8.99,'albumdefault.jpg'),(459,1,5,'Facelift',8.99,'albumdefault.jpg'),(460,1,51,'Greatest Hits I',8.99,'albumdefault.jpg'),(461,1,51,'Greatest Hits II',8.99,'albumdefault.jpg'),(462,1,51,'News Of The World',8.99,'albumdefault.jpg'),(463,1,52,'Greatest Kiss',8.99,'albumdefault.jpg'),(464,1,52,'Unplugged [Live]',8.99,'albumdefault.jpg'),(465,1,55,'Into The Light',8.99,'albumdefault.jpg'),(466,1,58,'Come Taste The Band',8.99,'albumdefault.jpg'),(467,1,58,'Deep Purple In Rock',8.99,'albumdefault.jpg'),(468,1,58,'Fireball',8.99,'albumdefault.jpg'),(469,1,58,'Machine Head',8.99,'albumdefault.jpg'),(470,1,58,'MK III The Final Concerts [Disc 1]',8.99,'albumdefault.jpg'),(471,1,58,'Purpendicular',8.99,'albumdefault.jpg'),(472,1,58,'Slaves And Masters',8.99,'albumdefault.jpg'),(473,1,58,'Stormbringer',8.99,'albumdefault.jpg'),(474,1,58,'The Battle Rages On',8.99,'albumdefault.jpg'),(475,1,58,'The Final Concerts (Disc 2)',8.99,'albumdefault.jpg'),(476,1,59,'Santana - As Years Go By',8.99,'albumdefault.jpg'),(477,1,59,'Santana Live',8.99,'albumdefault.jpg'),(478,1,59,'Supernatural',8.99,'albumdefault.jpg'),(479,1,76,'Chronicle, Vol. 1',8.99,'albumdefault.jpg'),(480,1,76,'Chronicle, Vol. 2',8.99,'albumdefault.jpg'),(481,1,8,'Audioslave',8.99,'albumdefault.jpg'),(482,1,82,'King For A Day Fool For A Lifetime',8.99,'albumdefault.jpg'),(483,1,84,'In Your Honor [Disc 1]',8.99,'albumdefault.jpg'),(484,1,84,'In Your Honor [Disc 2]',8.99,'albumdefault.jpg'),(485,1,84,'The Colour And The Shape',8.99,'albumdefault.jpg'),(486,1,88,'Appetite for Destruction',8.99,'albumdefault.jpg'),(487,1,88,'Use Your Illusion I',8.99,'albumdefault.jpg'),(488,1,90,'A Matter of Life and Death',8.99,'albumdefault.jpg'),(489,1,90,'Brave New World',8.99,'albumdefault.jpg'),(490,1,90,'Fear Of The Dark',8.99,'albumdefault.jpg'),(491,1,90,'Live At Donington 1992 (Disc 1)',8.99,'albumdefault.jpg'),(492,1,90,'Live At Donington 1992 (Disc 2)',8.99,'albumdefault.jpg'),(493,1,90,'Rock In Rio [CD2]',8.99,'albumdefault.jpg'),(494,1,90,'The Number of The Beast',8.99,'albumdefault.jpg'),(495,1,90,'The X Factor',8.99,'albumdefault.jpg'),(496,1,90,'Virtual XI',8.99,'albumdefault.jpg'),(497,1,92,'Emergency On Planet Earth',8.99,'albumdefault.jpg'),(498,1,94,'Are You Experienced?',8.99,'albumdefault.jpg'),(499,1,95,'Surfing with the Alien (Remastered)',8.99,'albumdefault.jpg'),(500,10,203,'The Best of Beethoven',8.99,'albumdefault.jpg'),(504,10,208,'Pachelbel: Canon & Gigue',8.99,'albumdefault.jpg'),(507,10,211,'Bach: Goldberg Variations',8.99,'albumdefault.jpg'),(508,10,212,'Bach: The Cello Suites',8.99,'albumdefault.jpg'),(509,10,213,'Handel: The Messiah (Highlights)',8.99,'albumdefault.jpg'),(513,10,217,'Haydn: Symphonies 99 - 104',8.99,'albumdefault.jpg'),(515,10,219,'A Soprano Inspired',8.99,'albumdefault.jpg'),(517,10,221,'Wagner: Favourite Overtures',8.99,'albumdefault.jpg'),(519,10,223,'Tchaikovsky: The Nutcracker',8.99,'albumdefault.jpg'),(520,10,224,'The Last Night of the Proms',8.99,'albumdefault.jpg'),(523,10,226,'Respighi:Pines of Rome',8.99,'albumdefault.jpg'),(524,10,226,'Strauss: Waltzes',8.99,'albumdefault.jpg'),(525,10,229,'Carmina Burana',8.99,'albumdefault.jpg'),(526,10,230,'A Copland Celebration, Vol. I',8.99,'albumdefault.jpg'),(527,10,231,'Bach: Toccata & Fugue in D Minor',8.99,'albumdefault.jpg'),(528,10,232,'Prokofiev: Symphony No.1',8.99,'albumdefault.jpg'),(529,10,233,'Scheherazade',8.99,'albumdefault.jpg'),(530,10,234,'Bach: The Brandenburg Concertos',8.99,'albumdefault.jpg'),(532,10,236,'Mascagni: Cavalleria Rusticana',8.99,'albumdefault.jpg'),(533,10,237,'Sibelius: Finlandia',8.99,'albumdefault.jpg'),(537,10,242,'Adams, John: The Chairman Dances',8.99,'albumdefault.jpg'),(539,10,245,'Berlioz: Symphonie Fantastique',8.99,'albumdefault.jpg'),(540,10,245,'Prokofiev: Romeo & Juliet',8.99,'albumdefault.jpg'),(542,10,247,'English Renaissance',8.99,'albumdefault.jpg'),(544,10,248,'Mozart: Symphonies Nos. 40 & 41',8.99,'albumdefault.jpg'),(546,10,250,'SCRIABIN: Vers la flamme',8.99,'albumdefault.jpg'),(548,10,255,'Bartok: Violin & Viola Concertos',8.99,'albumdefault.jpg'),(551,10,259,'South American Getaway',8.99,'albumdefault.jpg'),(552,10,260,'Górecki: Symphony No. 3',8.99,'albumdefault.jpg'),(553,10,261,'Purcell: The Fairy Queen',8.99,'albumdefault.jpg'),(556,10,264,'Weill: The Seven Deadly Sins',8.99,'albumdefault.jpg'),(558,10,266,'Szymanowski: Piano Works, Vol. 1',8.99,'albumdefault.jpg'),(559,10,267,'Nielsen: The Six Symphonies',8.99,'albumdefault.jpg'),(562,10,274,'Mozart: Chamber Music',8.99,'albumdefault.jpg'),(563,2,10,'The Best Of Billy Cobham',8.99,'albumdefault.jpg'),(564,2,197,'Quiet Songs',8.99,'albumdefault.jpg'),(565,2,202,'Worlds',8.99,'albumdefault.jpg'),(566,2,27,'Quanta Gente Veio ver--Bônus De Carnaval',8.99,'albumdefault.jpg'),(567,2,53,'Heart of the Night',8.99,'albumdefault.jpg'),(568,2,53,'Morning Dance',8.99,'albumdefault.jpg'),(569,2,6,'Warner 25 Anos',8.99,'albumdefault.jpg'),(570,2,68,'Miles Ahead',8.99,'albumdefault.jpg'),(571,2,68,'The Essential Miles Davis [Disc 1]',8.99,'albumdefault.jpg'),(572,2,68,'The Essential Miles Davis [Disc 2]',8.99,'albumdefault.jpg'),(573,2,79,'Outbreak',8.99,'albumdefault.jpg'),(574,2,89,'Blue Moods',8.99,'albumdefault.jpg'),(575,3,100,'Greatest Hits',8.99,'albumdefault.jpg'),(576,3,106,'Ace Of Spades',8.99,'albumdefault.jpg'),(577,3,109,'Motley Crue Greatest Hits',8.99,'albumdefault.jpg'),(578,3,11,'Alcohol Fueled Brewtality Live! [Disc 1]',8.99,'albumdefault.jpg'),(579,3,11,'Alcohol Fueled Brewtality Live! [Disc 2]',8.99,'albumdefault.jpg'),(580,3,114,'Tribute',8.99,'albumdefault.jpg'),(581,3,12,'Black Sabbath Vol. 4 (Remaster)',8.99,'albumdefault.jpg'),(582,3,12,'Black Sabbath',8.99,'albumdefault.jpg'),(583,3,135,'Mezmerize',8.99,'albumdefault.jpg'),(584,3,14,'Chemical Wedding',8.99,'albumdefault.jpg'),(585,3,50,'...And Justice For All',8.99,'albumdefault.jpg'),(586,3,50,'Black Album',8.99,'albumdefault.jpg'),(587,3,50,'Garage Inc. (Disc 1)',8.99,'albumdefault.jpg'),(588,3,50,'Garage Inc. (Disc 2)',8.99,'albumdefault.jpg'),(589,3,50,'Load',8.99,'albumdefault.jpg'),(590,3,50,'Master Of Puppets',8.99,'albumdefault.jpg'),(591,3,50,'ReLoad',8.99,'albumdefault.jpg'),(592,3,50,'Ride The Lightning',8.99,'albumdefault.jpg'),(593,3,50,'St. Anger',8.99,'albumdefault.jpg'),(594,3,7,'Plays Metallica By Four Cellos',8.99,'albumdefault.jpg'),(595,3,87,'Faceless',8.99,'albumdefault.jpg'),(596,3,88,'Use Your Illusion II',8.99,'albumdefault.jpg'),(597,3,90,'A Real Dead One',8.99,'albumdefault.jpg'),(598,3,90,'A Real Live One',8.99,'albumdefault.jpg'),(599,3,90,'Live After Death',8.99,'albumdefault.jpg'),(600,3,90,'No Prayer For The Dying',8.99,'albumdefault.jpg'),(601,3,90,'Piece Of Mind',8.99,'albumdefault.jpg'),(602,3,90,'Powerslave',8.99,'albumdefault.jpg'),(603,3,90,'Rock In Rio [CD1]',8.99,'albumdefault.jpg'),(604,3,90,'Rock In Rio [CD2]',8.99,'albumdefault.jpg'),(605,3,90,'Seventh Son of a Seventh Son',8.99,'albumdefault.jpg'),(606,3,90,'Somewhere in Time',8.99,'albumdefault.jpg'),(607,3,90,'The Number of The Beast',8.99,'albumdefault.jpg'),(608,3,98,'Living After Midnight',8.99,'albumdefault.jpg'),(609,4,196,'Cake: B-Sides and Rarities',8.99,'albumdefault.jpg'),(610,4,204,'Temple of the Dog',8.99,'albumdefault.jpg'),(611,4,205,'Carry On',8.99,'albumdefault.jpg'),(612,4,253,'Carried to Dust (Bonus Track Version)',8.99,'albumdefault.jpg'),(613,4,8,'Revelations',8.99,'albumdefault.jpg'),(614,6,133,'In Step',8.99,'albumdefault.jpg'),(615,6,137,'Live [Disc 1]',8.99,'albumdefault.jpg'),(616,6,137,'Live [Disc 2]',8.99,'albumdefault.jpg'),(618,6,81,'The Cream Of Clapton',8.99,'albumdefault.jpg'),(619,6,81,'Unplugged',8.99,'albumdefault.jpg'),(620,6,90,'Iron Maiden',8.99,'albumdefault.jpg'),(623,7,103,'Barulhinho Bom',8.99,'albumdefault.jpg'),(624,7,112,'Olodum',8.99,'albumdefault.jpg'),(625,7,113,'Acústico MTV',8.99,'albumdefault.jpg'),(626,7,113,'Arquivo II',8.99,'albumdefault.jpg'),(627,7,113,'Arquivo Os Paralamas Do Sucesso',8.99,'albumdefault.jpg'),(628,7,145,'Serie Sem Limite (Disc 1)',8.99,'albumdefault.jpg'),(629,7,145,'Serie Sem Limite (Disc 2)',8.99,'albumdefault.jpg'),(630,7,155,'Ao Vivo [IMPORT]',8.99,'albumdefault.jpg'),(631,7,16,'Prenda Minha',8.99,'albumdefault.jpg'),(632,7,16,'Sozinho Remix Ao Vivo',8.99,'albumdefault.jpg'),(633,7,17,'Minha Historia',8.99,'albumdefault.jpg'),(634,7,18,'Afrociberdelia',8.99,'albumdefault.jpg'),(635,7,18,'Da Lama Ao Caos',8.99,'albumdefault.jpg'),(636,7,20,'Na Pista',8.99,'albumdefault.jpg'),(637,7,201,'Duos II',8.99,'albumdefault.jpg'),(638,7,21,'Sambas De Enredo 2001',8.99,'albumdefault.jpg'),(639,7,21,'Vozes do MPB',8.99,'albumdefault.jpg'),(640,7,24,'Chill: Brazil (Disc 1)',8.99,'albumdefault.jpg'),(641,7,27,'Quanta Gente Veio Ver (Live)',8.99,'albumdefault.jpg'),(642,7,37,'The Best of Ed Motta',8.99,'albumdefault.jpg'),(643,7,41,'Elis Regina-Minha História',8.99,'albumdefault.jpg'),(644,7,42,'Milton Nascimento Ao Vivo',8.99,'albumdefault.jpg'),(645,7,42,'Minas',8.99,'albumdefault.jpg'),(646,7,46,'Jorge Ben Jor 25 Anos',8.99,'albumdefault.jpg'),(647,7,56,'Meus Momentos',8.99,'albumdefault.jpg'),(648,7,6,'Chill: Brazil (Disc 2)',8.99,'albumdefault.jpg'),(649,7,72,'Vinicius De Moraes',8.99,'albumdefault.jpg'),(651,7,77,'Cássia Eller - Sem Limite [Disc 1]',8.99,'albumdefault.jpg'),(652,7,80,'Djavan Ao Vivo - Vol. 02',8.99,'albumdefault.jpg'),(653,7,80,'Djavan Ao Vivo - Vol. 1',8.99,'albumdefault.jpg'),(654,7,81,'Unplugged',8.99,'albumdefault.jpg'),(655,7,83,'Deixa Entrar',8.99,'albumdefault.jpg'),(656,7,86,'Roda De Funk',8.99,'albumdefault.jpg'),(657,7,96,'Jota Quest-1995',8.99,'albumdefault.jpg'),(659,7,99,'Mais Do Mesmo',8.99,'albumdefault.jpg'),(660,8,100,'Greatest Hits',8.99,'albumdefault.jpg'),(661,8,151,'UB40 The Best Of - Volume Two [UK]',8.99,'albumdefault.jpg'),(662,8,19,'Acústico MTV [Live]',8.99,'albumdefault.jpg'),(663,8,19,'Cidade Negra - Hits',8.99,'albumdefault.jpg'),(665,9,21,'Axé Bahia 2001',8.99,'albumdefault.jpg'),(666,9,252,'Frank',8.99,'albumdefault.jpg'),(667,5,276,'Le Freak',8.99,'albumdefault.jpg'),(668,5,278,'MacArthur Park Suite',8.99,'albumdefault.jpg'),(669,5,277,'Ring My Bell',8.99,'albumdefault.jpg');
/*!40000 ALTER TABLE `albums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artists`
--

DROP TABLE IF EXISTS `artists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artists` (
  `Artist_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(120) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`Artist_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=279 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artists`
--

LOCK TABLES `artists` WRITE;
/*!40000 ALTER TABLE `artists` DISABLE KEYS */;
INSERT INTO `artists` VALUES (1,'AC/DC'),(2,'Accept'),(3,'Aerosmith'),(4,'Alanis Morissette'),(5,'Alice In Chains'),(6,'Antônio Carlos Jobim'),(7,'Apocalyptica'),(8,'Audioslave'),(10,'Billy Cobham'),(11,'Black Label Society'),(12,'Black Sabbath'),(14,'Bruce Dickinson'),(15,'Buddy Guy'),(16,'Caetano Veloso'),(17,'Chico Buarque'),(18,'Chico Science & Nação Zumbi'),(19,'Cidade Negra'),(20,'Cláudio Zoli'),(21,'Various Artistsss'),(22,'Led Zeppelin'),(23,'Frank Zappa & Captain Beefheart'),(24,'Marcos Valle'),(27,'Gilberto Gil'),(37,'Ed Motta'),(41,'Elis Regina'),(42,'Milton Nascimento'),(46,'Jorge Ben'),(50,'Metallica'),(51,'Queen'),(52,'Kiss'),(53,'Spyro Gyra'),(55,'David Coverdale'),(56,'Gonzaguinha'),(58,'Deep Purple'),(59,'Santana'),(68,'Miles Davis'),(72,'Vinícius De Moraes'),(76,'Creedence Clearwater Revival'),(77,'Cássia Eller'),(79,'Dennis Chambers'),(80,'Djavan'),(81,'Eric Clapton'),(82,'Faith No More'),(83,'Falamansa'),(84,'Foo Fighters'),(86,'Funk Como Le Gusta'),(87,'Godsmack'),(88,'Guns N\' Roses'),(89,'Incognito'),(90,'Iron Maiden'),(92,'Jamiroquai'),(94,'Jimi Hendrix'),(95,'Joe Satriani'),(96,'Jota Quest'),(98,'Judas Priest'),(99,'Legião Urbana'),(100,'Lenny Kravitz'),(101,'Lulu Santos'),(102,'Marillion'),(103,'Marisa Monte'),(105,'Men At Work'),(106,'Motörhead'),(109,'Mötley Crüe'),(110,'Nirvana'),(111,'O Terço'),(112,'Olodum'),(113,'Os Paralamas Do Sucesso'),(114,'Ozzy Osbourne'),(115,'Page & Plant'),(117,'Paul D\'Ianno'),(118,'Pearl Jam'),(120,'Pink Floyd'),(124,'R.E.M.'),(126,'Raul Seixas'),(127,'Red Hot Chili Peppers'),(128,'Rush'),(130,'Skank'),(132,'Soundgarden'),(133,'Stevie Ray Vaughan & Double Trouble'),(134,'Stone Temple Pilots'),(135,'System Of A Down'),(136,'Terry Bozzio, Tony Levin & Steve Stevens'),(137,'The Black Crowes'),(139,'The Cult'),(140,'The Doors'),(141,'The Police'),(142,'The Rolling Stones'),(144,'The Who'),(145,'Tim Maia'),(150,'U2'),(151,'UB40'),(152,'Van Halen'),(153,'Velvet Revolver'),(155,'Zeca Pagodinho'),(157,'Dread Zeppelin'),(179,'Scorpions'),(196,'Cake'),(197,'Aisha Duo'),(200,'The Posies'),(201,'Luciana Souza/Romero Lubambo'),(202,'Aaron Goldberg'),(203,'Nicolaus Esterhazy Sinfonia'),(204,'Temple of the Dog'),(205,'Chris Cornell'),(206,'Alberto Turco & Nova Schola Gregoriana'),(208,'English Concert & Trevor Pinnock'),(211,'Wilhelm Kempff'),(212,'Yo-Yo Ma'),(213,'Scholars Baroque Ensemble'),(217,'Royal Philharmonic Orchestra & Sir Thomas Beecham'),(219,'Britten Sinfonia, Ivor Bolton & Lesley Garrett'),(221,'Sir Georg Solti & Wiener Philharmoniker'),(223,'London Symphony Orchestra & Sir Charles Mackerras'),(224,'Barry Wordsworth & BBC Concert Orchestra'),(226,'Eugene Ormandy'),(229,'Boston Symphony Orchestra & Seiji Ozawa'),(230,'Aaron Copland & London Symphony Orchestra'),(231,'Ton Koopman'),(232,'Sergei Prokofiev & Yuri Temirkanov'),(233,'Chicago Symphony Orchestra & Fritz Reiner'),(234,'Orchestra of The Age of Enlightenment'),(236,'James Levine'),(237,'Berliner Philharmoniker & Hans Rosbaud'),(238,'Maurizio Pollini'),(240,'Gustav Mahler'),(242,'Edo de Waart & San Francisco Symphony'),(244,'Choir Of Westminster Abbey & Simon Preston'),(245,'Michael Tilson Thomas & San Francisco Symphony'),(247,'The King\'s Singers'),(248,'Berliner Philharmoniker & Herbert Von Karajan'),(250,'Christopher O\'Riley'),(251,'Fretwork'),(252,'Amy Winehouse'),(253,'Calexico'),(255,'Yehudi Menuhin'),(258,'Les Arts Florissants & William Christie'),(259,'The 12 Cellists of The Berlin Philharmonic'),(260,'Adrian Leaper & Doreen de Feis'),(261,'Roger Norrington, London Classical Players'),(264,'Kent Nagano and Orchestre de l\'Opéra de Lyon'),(265,'Julian Bream'),(266,'Martin Roscoe'),(267,'Göteborgs Symfoniker & Neeme Järvi'),(270,'Gerald Moore'),(271,'Mela Tenenbaum, Pro Musica Prague & Richard Kapp'),(274,'Nash Ensemble'),(276,'Chic'),(277,'Anita Ward'),(278,'Donna Summer');
/*!40000 ALTER TABLE `artists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genres`
--

DROP TABLE IF EXISTS `genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genres` (
  `Genre_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(120) CHARACTER SET utf8 NOT NULL,
  `Description` varchar(4000) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`Genre_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genres`
--

LOCK TABLES `genres` WRITE;
/*!40000 ALTER TABLE `genres` DISABLE KEYS */;
INSERT INTO `genres` VALUES (1,'Rock','Rock and Roll is a form of rock music developed in the 1950s and 1960s. Rock music combines many kinds of music from the United States, such as country music, folk music, church music, work songs, blues and jazz.'),(2,'Jazz','Jazz is a type of music which was invented in the United States. Jazz music combines African-American music with European music. Some common jazz instruments include the saxophone, trumpet, piano, double bass, and drums.'),(3,'Metal','Heavy Metal is a loud, aggressive style of Rock music. The bands who play heavy-metal music usually have one or two guitars, a bass guitar and drums. In some bands, electronic keyboards, organs, or other instruments are used. Heavy metal songs are loud and powerful-sounding, and have strong rhythms that are repeated. There are many different types of Heavy Metal, some of which are described below. Heavy metal bands sometimes dress in jeans, leather jackets, and leather boots, and have long hair. Heavy metal bands sometimes behave in a dramatic way when they play their instruments or sing. However, many heavy metal bands do not like to do this.'),(4,'Alternative','Alternative rock is a type of rock music that became popular in the 1980s and became widely popular in the 1990s. Alternative rock is made up of various subgenres that have come out of the indie music scene since the 1980s, such as grunge, indie rock, Britpop, gothic rock, and indie pop. These genres are sorted by their collective types of punk, which laid the groundwork for alternative music in the 1970s.'),(5,'Disco','Disco is a style of pop music that was popular in the mid-1970s. Disco music has a strong beat that people can dance to. People usually dance to disco music at bars called disco clubs. The word \"disco\" is also used to refer to the style of dancing that people do to disco music, or to the style of clothes that people wear to go disco dancing. Disco was at its most popular in the United States and Europe in the 1970s and early 1980s. Disco was brought into the mainstream by the hit movie Saturday Night Fever, which was released in 1977. This movie, which starred John Travolta, showed people doing disco dancing. Many radio stations played disco in the late 1970s.'),(6,'Blues','The blues is a form of music that started in the United States during the start of the 20th century. It was started by former African slaves from spirituals, praise songs, and chants. The first blues songs were called Delta blues. These songs came from the area near the mouth of the Mississippi River.'),(7,'Latin','Latin American music is the music of all countries in Latin America (and the Caribbean) and comes in many varieties. Latin America is home to musical styles such as the simple, rural conjunto music of northern Mexico, the sophisticated habanera of Cuba, the rhythmic sounds of the Puerto Rican plena, the symphonies of Heitor Villa-Lobos, and the simple and moving Andean flute. Music has played an important part recently in Latin America\'s politics, the nueva canción movement being a prime example. Latin music is very diverse, with the only truly unifying thread being the use of Latin-derived languages, predominantly the Spanish language, the Portuguese language in Brazil, and to a lesser extent, Latin-derived creole languages, such as those found in Haiti.'),(8,'Reggae','Reggae is a music genre first developed in Jamaica in the late 1960s. While sometimes used in a broader sense to refer to most types of Jamaican music, the term reggae more properly denotes a particular music style that originated following on the development of ska and rocksteady.'),(9,'Pop','Pop music is a music genre that developed from the mid-1950s as a softer alternative to rock \'n\' roll and later to rock music. It has a focus on commercial recording, often oriented towards a youth market, usually through the medium of relatively short and simple love songs. While these basic elements of the genre have remained fairly constant, pop music has absorbed influences from most other forms of popular music, particularly borrowing from the development of rock music, and utilizing key technological innovations to produce new variations on existing themes.'),(10,'Classical','Classical music is a very general term which normally refers to the standard music of countries in the Western world. It is music that has been composed by musicians who are trained in the art of writing music (composing) and written down in music notation so that other musicians can play it. Classical music can also be described as \"art music\" because great art (skill) is needed to compose it and to perform it well. Classical music differs from pop music because it is not made just in order to be popular for a short time or just to be a commercial success.');
/*!40000 ALTER TABLE `genres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_details` (
  `OrderDetail_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Order_ID` int(11) NOT NULL,
  `Album_ID` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `UnitPrice` double NOT NULL,
  PRIMARY KEY (`OrderDetail_ID`),
  KEY `ORDERDETAIL_ORDER_FK` (`Order_ID`),
  KEY `ORDERDETAIL_ALBUM_FK` (`Album_ID`),
  CONSTRAINT `ORDERDETAIL_ALBUM_FK` FOREIGN KEY (`Album_ID`) REFERENCES `albums` (`Album_ID`),
  CONSTRAINT `ORDERDETAIL_ORDER_FK` FOREIGN KEY (`Order_ID`) REFERENCES `orders` (`Order_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
INSERT INTO `order_details` VALUES (1,1,386,2,8.99),(2,1,387,1,8.99),(3,2,386,1,8.99),(4,2,387,2,8.99),(5,3,386,1,8.99);
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `Order_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Order_Date` datetime NOT NULL,
  `User_Name` varchar(20) NOT NULL,
  `First_Name` varchar(160) CHARACTER SET utf8 NOT NULL,
  `Last_Name` varchar(160) CHARACTER SET utf8 NOT NULL,
  `Address` varchar(70) CHARACTER SET utf8 NOT NULL,
  `City` varchar(40) CHARACTER SET utf8 NOT NULL,
  `State` varchar(40) CHARACTER SET utf8 NOT NULL,
  `PostalCode` varchar(10) CHARACTER SET utf8 NOT NULL,
  `Country` varchar(40) CHARACTER SET utf8 NOT NULL,
  `Phone` varchar(24) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(160) CHARACTER SET utf8 NOT NULL,
  `Total` double NOT NULL,
  PRIMARY KEY (`Order_ID`),
  KEY `ORDER_ACCOUNT_FK` (`User_Name`),
  CONSTRAINT `ORDER_ACCOUNT_FK` FOREIGN KEY (`User_Name`) REFERENCES `accounts` (`User_Name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'2017-11-28 15:41:24','cus1','Dinh','My','123 abc','TPHCM','None','123ABC','Viet Nam','1234567890','123@gmail.com',26.97),(2,'2017-12-05 16:18:12','cus1','Dinh','My','123 abc','TPHCM','None','123ABC','Vietnam','1234567890','123@gmail.com',26.97),(3,'2017-12-06 16:53:00','cus2','abc','acb','123 abc','TPHCM','None','123ABC','Vietnam','1234567890','123@gmail.com',8.99);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `Role_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) NOT NULL,
  PRIMARY KEY (`Role_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'CUSTOMER'),(2,'ADMIN');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-13 15:07:46
