import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent }   from './login/login.component';
import { AlbumComponent } from './album/album.component';
import { AddAllbumComponent } from './add-allbum/add-allbum.component';
import { EditAlbumComponent } from './edit-album/edit-album.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'album', component: AlbumComponent },
  { path: 'addalbum', component: AddAllbumComponent },
  { path: 'editalbum/:id', component: EditAlbumComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
