import { Component } from '@angular/core';
import { LoginService } from './service/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Music Store';
  token = sessionStorage.getItem('token');
  role = sessionStorage.getItem('role');
  username = sessionStorage.getItem('username');
  constructor(private loginService: LoginService){
  }

  ngOnInit() {
      this.token = sessionStorage.getItem('token');

      this.loginService.getAccountDetail().toPromise()
          .then(res => {
            return res.json();
          })
          .then((data) => {
              console.log('promise:', data);
              sessionStorage.setItem('username', data.username);
              console.log('username:', sessionStorage.getItem('username'));
              sessionStorage.setItem('role', data.role.name);
              console.log('role:', sessionStorage.getItem('role'));
              this.role = sessionStorage.getItem('role');
              this.username = sessionStorage.getItem('username');
          });

  }
  logout(){
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('role');
    this.token = sessionStorage.getItem('token');
    this.role = sessionStorage.getItem('role');
    this.username = sessionStorage.getItem('username');
    window.location.href='/login';
  }
}
