import { Account } from './model/account';

export const ACCOUNTS: Account[] = [
  { id: 1, username: 'Mr. Nice', password: '123', email: '123', userrole:'Admin' },
  { id: 12, username: 'Narco', password: '123', email: '123', userrole:'Admin' },
  { id: 13, username: 'Bombasto', password: '123', email: '123', userrole:'Admin' },
  { id: 14, username: 'Celeritas', password: '123', email: '123', userrole:'Admin' },
  { id: 15, username: 'Magneta', password: '123', email: '123', userrole:'Admin' },
  { id: 16, username: 'RubberMan', password: '123', email: '123', userrole:'Admin' },
  { id: 17, username: 'Dynama' , password: '123', email: '123', userrole:'Admin'},
  { id: 18, username: 'Dr IQ', password: '123', email: '123', userrole:'Admin' },
  { id: 19, username: 'Magma', password: '123', email: '123', userrole:'Admin' },
  { id: 20, username: 'Tornado', password: '123', email: '123', userrole:'Admin' }
];
