import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './service/login.service';
import { AlbumService } from './service/album.service';
import { GenreService } from './service/genre.service';
import { ArtistService } from './service/artist.service';

import { AppRoutingModule }     from './app-routing.module';
import { AlbumComponent } from './album/album.component';
import { AddAllbumComponent } from './add-allbum/add-allbum.component';
import { EditAlbumComponent } from './edit-album/edit-album.component';

import {DetailsUploadComponent} from './upload/details-upload.component';
import {FormUploadComponent} from './upload/form-upload.component';
import {ListUploadComponent} from './upload/list-upload.component';
import {UploadFileService} from './service/upload-file.service';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AlbumComponent,
    AddAllbumComponent,
    EditAlbumComponent,
    DetailsUploadComponent,
    FormUploadComponent,
    ListUploadComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [
    LoginService,
    AlbumService,
    GenreService,
    ArtistService,
    UploadFileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
