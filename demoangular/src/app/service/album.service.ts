import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { Album } from '../model/album';
import {Http} from "@angular/http";
@Injectable()
export class AlbumService {

  constructor(private http: Http) { }
  public getAlbumList(){
    return this.http.get('http://localhost:8080/api/album/albumList');
  }

  public getAlbumDetail(id: number){
    return this.http.get('http://localhost:8080/api/album/albumdetail?id=' + id);
  }

  public deleteAlbum(id){
    return this.http.delete('http://localhost:8080/api/album/delete?id=' + id);

  }

  public addAlbum(album: Album){
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
     });
    let options = new RequestOptions({ headers: headers });
    console.log('File name service', album.imageUrl);//file name
    let body = {
      title: album.title,
      priceString: album.priceString,
      genreID: album.genre,
      artistID: album.artist,
      imageUrl: album.imageUrl,
    };
    return this.http.post('http://localhost:8080/api/album/add',body, options);
  }

  public editAlbum(album: Album){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    console.log('edit - album artist', album.artist);
    let body = {
      title: album.title,
      priceString: album.priceString,
      genreID: album.genre,
      artistID: album.artist,
      imageUrl: album.imageUrl,
    };
    return this.http.put('http://localhost:8080/api/album/edit?id=' + album.id ,body, options);

  }
}
