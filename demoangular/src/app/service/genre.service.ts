import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import { Genre } from '../model/genre';
import {Http} from "@angular/http";
@Injectable()
export class GenreService {

  constructor(private http: Http) { }
  public getGenreList(){
    return this.http.get('http://localhost:8080/api/genre/genreList');

  }

}
