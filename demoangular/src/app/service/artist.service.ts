import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import { Genre } from '../model/genre';
import {Http} from "@angular/http";
@Injectable()
export class ArtistService {

  constructor(private http: Http) { }
  public getArtistList(){
    return this.http.get('http://localhost:8080/api/artist/artistList');

  }

}
