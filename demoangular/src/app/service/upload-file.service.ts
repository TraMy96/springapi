import {Injectable} from '@angular/core';
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UploadFileService {

  constructor(private http: HttpClient) {}

  pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
    let formdata: FormData = new FormData();

    formdata.append('file', file);

    const req = new HttpRequest('POST', 'http://localhost:8080/api/upload/post', formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }

  getFiles(fileName: String): Observable<any> {
    return this.http.get('http://localhost:8080/api/upload/getallfiles?filename=' + fileName)
  }
  getFileSrc(fileName: String) {
    console.log('getfile');
    return this.http.get('http://localhost:8080/api/upload/files/' + fileName);
  }

  /*getImage(filename:String):Observable<any> {
    return this.http.get(imagesUrl + "getImage/" + filename)
      .map(this.extractUrl)
      .catch(this.handleError);
  }
  extractUrl(res:Response):string {
    return res.url;
  }*/
}
