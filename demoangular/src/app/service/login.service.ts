import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Album } from '../model/album';
import { Account } from '../model/account';

import {Http} from "@angular/http";
import { Headers, RequestOptions } from '@angular/http';


@Injectable()
export class LoginService {

  constructor(private http: Http) { }
  public logIn(account){
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    /*let body = {
      username: account.username,
      password: account.password,
    };*/
    let body = new URLSearchParams();
    body.set('username', account.username);
    body.set('password', account.password);
    return this.http.post('http://localhost:8080/api/login', body.toString(), options);
  }

  public getAccountDetail(){
    let headers = new Headers({ 'Authorization': sessionStorage.getItem('token') });
    let options = new RequestOptions({ headers: headers });

    return this.http.get('http://localhost:8080/api/account/accountDetail', options);


  }
}
