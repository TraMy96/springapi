import { Component, OnInit } from '@angular/core';
import { Album } from '../model/album';
import { HttpClient } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { LoginService } from '../service/login.service';
import { AlbumService } from '../service/album.service';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {
  albumList: Album[] =[];
  token = sessionStorage.getItem('token');
  constructor(private albumService: AlbumService) { }

  ngOnInit(): Promise<any>{
    if (!sessionStorage.getItem('token'))
    {
      window.location.href='/login';
      return;
    }
    return this.albumService.getAlbumList().toPromise()
        .then(res => {
          return res.json();
        })
        .then((data) => {
            console.log('promise:', data);
            for (let i = 0; i < data.length; i++){
              this.albumList.push(
                {
                  id: data[i].album.id,
                  title: data[i].album.title,
                  price: data[i].album.price,
                  priceString: '' + data[i].album.price,
                  imageUrl: data[i].album.imageUrl,
                  genre: data[i].album.genre.name,
                  artist: data[i].album.artist.name,
                }
              );
            }
            console.log('albumList:', this.albumList);
        });

    /*return this.loginService.getAlbumList().toPromise().subscribe(data => {
      console.log('component:', data);
      for (let i = 0; i < data.length; i++){
        album: Album;
        album.id = data[i].album.id;
        album.title = data[i].album.title;
        album.price = data[i].album.price;
        album.imageUrl = data[i].album.imageUrl;
        album.genre = data[i].album.genre.name;
        album.artist = data[i].album.artist.name;
        this.albumList.push(album);
      }
    });*/
    //  console.log('albumList:', this.albumList);
  }

  deleteAlbum(id): Promise<any> {

    if(confirm("Do you want to delete this album?"))
        {
          console.log('delete', id);
          return this.albumService.deleteAlbum(id).toPromise()
              .then(res => {
                return res.json();
              })
              .then((data) => {
                  console.log('promise:', data);
                  for (let i = 0; i <   this.albumList.length; i++){
                    if (this.albumList[i].id == id){
                      this.albumList.splice(i, 1);
                    }

                  }
                  console.log('albumList:', this.albumList);
              });
        }else{
          return null;
       }

  }
}
