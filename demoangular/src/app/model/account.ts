export class Account {
  id: number;
  username: string;
  password: string;
  userrole: string;
  email: string;
}
