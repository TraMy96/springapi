export class Album {
  id: number;
  title: string;
  price: number;
  priceString: string;
  imageUrl: string;
  genre: string;
  artist: string;
}
