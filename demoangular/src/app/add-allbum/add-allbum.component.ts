import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Album } from '../model/album';
import { Genre } from '../model/genre';
import { Artist } from '../model/artist';

import { HttpClient, HttpResponse, HttpEventType } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { AlbumService } from '../service/album.service';
import { GenreService } from '../service/genre.service';
import { ArtistService } from '../service/artist.service';
import { UploadFileService } from '../service/upload-file.service';

@Component({
  selector: 'app-add-allbum',
  templateUrl: './add-allbum.component.html',
  styleUrls: ['./add-allbum.component.css']
})
export class AddAllbumComponent implements OnInit {
  token = sessionStorage.getItem('token');
  genreList: Genre[] = [];
  artistList: Artist[] = [];
  album: Album ={
    id: 1,
    title: '',
    price: 0,
    priceString: '0',
    genre: '1',
    artist: '1',
    imageUrl: 'defaultalbum.jpg'
  };
  messagesError = {
    priceString: "",
    title: "",
    imageUrl: ""
  };

  selectedFiles: FileList;
  currentFileUpload: File;
  imageSrc: File;
  constructor(private albumService: AlbumService, private genreService: GenreService,
    private artistService: ArtistService, private router:Router, private uploadService: UploadFileService) { }

  ngOnInit(){
    if (!sessionStorage.getItem('token'))
    {
      window.location.href='/login';
      return;
    }
  this.genreService.getGenreList().toPromise()
    .then(res => {
      return res.json();
    })
    .then((data) => {
        for (let i = 0; i < data.length; i++){
          this.genreList.push(
            {
              id: data[i].id,
              name: data[i].name,
              description: data[i].description,
            }
          );
        }
        console.log('genreList:', this.genreList);
    });

    this.artistService.getArtistList().toPromise()
      .then(res => {
        return res.json();
      })
      .then((data) => {
          for (let i = 0; i < data.length; i++){
            this.artistList.push(
              {
                id: data[i].id,
                name: data[i].name,
              }
            );
          }
          console.log('artistList:', this.artistList);
      });
  }

  setGenre(genreID) {
    this.album.genre = genreID;
    console.log('genreID', genreID);
  }
  setArtist(artistID) {
    this.album.artist = artistID;
  }

  selectFile(event) {
    let file;
    if (event.target.files.item(0))
      file = event.target.files.item(0);


    if (file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
      if (event.target.files && event.target.files[0]) {
          var reader = new FileReader();

          reader.onload = (event:any) => {
            this.imageSrc = event.target.result;
          }

          reader.readAsDataURL(event.target.files[0]);
    } else {
      alert('invalid format!');
    }
  }
}
  add() {
    console.log('album info', this.album);
    if (this.selectedFiles){
      this.currentFileUpload = this.selectedFiles.item(0);
      this.uploadService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
        } else if (event instanceof HttpResponse) {
          console.log('File is completely uploaded!', event.body);//file name
          this.album.imageUrl = '' + event.body;
          console.log('File name', this.album.imageUrl);//file name
          //Save
          this.albumService.addAlbum( this.album).toPromise()
            .then(res => {
              return res.json();
            })
            .then((data) => {
                if (data.result){
                  console.log('has Error:', data.result);
                  this.messagesError = {
                    priceString: "",
                    title: "",
                    imageUrl: ""
                  };
                  for(let i = 0; i < data.result.length; i++){
                    let error = data.result[i].field;
                    if (error == 'priceString'){
                        console.log('priceString:', data.result[i]);
                      this.messagesError.priceString = 'Price is not valid!';
                    }
                    if (error == 'title'){
                      console.log('title:', data.result[i]);
                      this.messagesError.title = 'Title is not valid!';
                    }
                    if (error == 'imageUrl'){
                      console.log('imageUrl:', data.result[i]);

                      this.messagesError.imageUrl = 'Image Url is not valid!';
                    }
                  }
                }
                else{
                  console.log('added:', data);
                  this.router.navigate(["/album"]);
                }

            });
        }
      })

      this.selectedFiles = undefined;
    }
    else{
      this.albumService.addAlbum( this.album).toPromise()
        .then(res => {
          return res.json();
        })
        .then((data) => {
            if (data.result){
              console.log('has Error:', data.result);
              this.messagesError = {
                priceString: "",
                title: "",
                imageUrl: ""
              };
              for(let i = 0; i < data.result.length; i++){
                let error = data.result[i].field;
                if (error == 'priceString'){
                    console.log('priceString:', data.result[i]);
                  this.messagesError.priceString = 'Price is not valid!';
                }
                if (error == 'title'){
                  console.log('title:', data.result[i]);
                  this.messagesError.title = 'Title is not valid!';
                }
                if (error == 'imageUrl'){
                  console.log('imageUrl:', data.result[i]);

                  this.messagesError.imageUrl = 'Image Url is not valid!';
                }
              }
            }
            else{
              console.log('added:', data);
              this.router.navigate(["/album"]);
            }

        });
    }
  }

}
