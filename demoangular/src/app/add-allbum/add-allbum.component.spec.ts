import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAllbumComponent } from './add-allbum.component';

describe('AddAllbumComponent', () => {
  let component: AddAllbumComponent;
  let fixture: ComponentFixture<AddAllbumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAllbumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAllbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
