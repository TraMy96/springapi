import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { Account } from '../model/account';

import { HttpClient } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { LoginService } from '../service/login.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    account: Account = {
      id: 0,
      username: '',
      password: '',
      userrole: '',
      email: '',
    };
    messagesError: string;
    constructor(private http: HttpClient, private loginService: LoginService, private router:Router){
    }

  ngOnInit() {
  }

  login(){

        this.loginService.logIn( this.account)
        .map((data: any) => data)
        .subscribe(
                (data: any) => {
                  console.log('headers:', data.headers.get('authorization'));
                  sessionStorage.setItem('token', data.headers.get('authorization'));
                  //this.router.navigate(["/album"]);
                  window.location.href='/album';
                },
                err => {
                  console.log('error', err);
                  this.messagesError = "Username and Password invalid!";
                }, // error
                () => console.log('getUserStatus Complete') // complete
            );

      var token = sessionStorage.getItem('token'); // your token
      console.log('token:', token);
  }
}
